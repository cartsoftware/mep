using System;
using System.Collections.Generic;
using System.Text;

namespace NetCoreConsoleClient
{
    class Telemetry
    {
        public Double pressure { get; set; }
        public Double temperature { get; set; }
        public DateTime time { get; set; }
    }
}
